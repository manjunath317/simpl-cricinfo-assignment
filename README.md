# simpl-cricinfo-assignment

Requirements:
It's the finals of the T20 Cup! Bengaluru and Chennai, neighbours and fierce rivals, are
fighting it out for the title. Bengaluru's star batsman Kirat is at the crease. Can he win it for
Bengaluru? Write code to simulate the last 4 overs of the match.
It's the last 4 overs of the match. Bengaluru needs 40 runs to win and with 4 wickets left. Each
player has a different probability for scoring runs. Your coding problem is to simulate the match,
ball by ball. The match simulation will require you to use a weighted random number generation
based on probability to determine the runs scored per ball.

Rules of the game:
● Batsmen change strike end of every over. They also change strike when they score a
1,3 or 5
● When a player gets out, the new player comes in at the same position.
● Assume only legal balls are bowled (no wides, no no-balls etc..). Therefore an over is
always 6 balls.

1. Objective:

Objective of this project is to simulate the cricket match for below scenario.
    a. Team1 (Chennai) batting is already done. Say a score of x=200.
    b. Team2 (Bengaluru) batting is in progress.
    c. Team2 only 4 overs left
    d. Team2 runs to win is 40
    e. Team2 current score is: 160 (based on runs to win and total score from team1) 
    e. Simulation is to check if Virat is on crease whether he win it for Bengaluru.
    f. Run made for each ball is simulated using weighted random number generation based on probability


2. Pre-requisite
    a. Java 8
    b. Maven
    c. jUnit 3.8.1
    d. Mocknet library for generating weighted random number with probability.
	e. git

3. Checkout
Checkout or clone source code from link: git clone git@bitbucket.org:manjunath317/simpl-cricinfo-assignment.git


4. Compile, Build and run Test suite
Run a command to compile and build the project 
    > mvn clean package

5. Run Application
Run a command to compile, build, test and execute class file (APP.java)
    > mvn exec:java


Sample Score Board Output
Bengaluru won by 1 wicket and 2 balls remaining
Virat - 12 (6 balls)
Dhoni - 25 (11 balls)
Bhumra - 2* (3 balls)
Shashi - 2* (2 balls)

Sample commentary
4 overs left. 40 runs to win
0.1 Virat scores 1 run
0.2 Dhoni scores 4 runs
0.3 Dhoni scores 1 run
0.4 Virat scores 2 runs
0.5 Virat scores 3 runs
0.6 Dhoni scores 1 run
3 overs left. 28 runs to win
1.1 Dhoni scores 2 runs
