package com.simpl.com.simpl;

import com.simpl.assignment.AppConstants;
import com.simpl.assignment.ScoreBoard;
import com.simpl.assignment.dto.Team;
import com.simpl.assignment.utils.Utils;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName ){
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite(){
        return new TestSuite(AppTest.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp(){
        assertTrue( true );
    }
    
    
    public void testTeam() {
    	Utils cricInfoUtils = new Utils();
		cricInfoUtils.intTeams();
		ScoreBoard sb = ScoreBoard.getInstance();
		Team team1 = sb.getTeam1();
		Team team2 = sb.getTeam2();
		assertTrue(team1.getPlayers().size() == 11);
		assertTrue(team2.getPlayers().size() == 11);
    }
    
    public void testTeamScore() {
    	Utils cricInfoUtils = new Utils();
		cricInfoUtils.intTeams();
		ScoreBoard sb = ScoreBoard.getInstance();
		Team team1 = sb.getTeam1();
		Team team2 = sb.getTeam2();
		assertTrue(team1.getRuns() > team2.getRuns());
    }
    
    public void testRunsToWin() {
    	Utils cricInfoUtils = new Utils();
		cricInfoUtils.intTeams();
		ScoreBoard sb = ScoreBoard.getInstance();
		Team team1 = sb.getTeam1();
		Team team2 = sb.getTeam2();
    	int runsToWin = team1.getRuns() - team2.getRuns();
		assertTrue(runsToWin == 40);
    }
    
    public void testTeam2OversLeft() {
    	Utils cricInfoUtils = new Utils();
		cricInfoUtils.intTeams();
		ScoreBoard sb = ScoreBoard.getInstance();
		Team team2 = sb.getTeam2();
		int oversLeft = AppConstants.TOTAL_OVERS - team2.getOvers();
		assertTrue(oversLeft == 4);
    }
    
    public void testTeam2WicketsLeft() {
    	Utils cricInfoUtils = new Utils();
		cricInfoUtils.intTeams();
		ScoreBoard sb = ScoreBoard.getInstance();
		Team team2 = sb.getTeam2();
		int wicketsLeft = team2.getPlayers().size() - team2.getWickets();
		assertTrue(wicketsLeft == 4);
    }
    
    public void testWeightedRandomNumber() {
		try {
			int x = Utils.getRun("Virat");
			if(x >100 && x < 200) {
				System.out.println("Virat scores 0 run");
			}
			if(x >=1 && x <= 6) {
				System.out.println("Virat scores run: "+x);
			}
			if(x >=300 && x <=400) {
				System.out.println("Virat is OUT");
			}
			// test if generated x is dot ball
			assertTrue(x > 0);
		}catch(Exception ex) {
			// do nothing
		}
    }
    
    
    
    
}
