package com.simpl.assignment;

import java.util.List;

import com.simpl.assignment.dto.Commentry;
import com.simpl.assignment.dto.Player;
import com.simpl.assignment.dto.Team;
import com.simpl.assignment.utils.Utils;

/**
 * This class used to simulate the last few overs left by team2 with the help of weighted random number generation based on
 * probability. Also to diplay the boards of score and ball by ball commentry details.
 * @author Manjunath Jakkandi
 *
 */
public class SimplCricInfo {
	private Commentry commentry;
	
	public SimplCricInfo(Commentry commentry) {
		this.commentry = commentry;
	}
	
	public Commentry getCommentry() {
		return commentry;
	}

	public void setCommentry(Commentry commentry) {
		this.commentry = commentry;
	}

	/**
	 * This method used to trigger commentry display if change in data of overs, runs and wickets using observer pattern.
	 * probability using MockNeat library.
	 * 
	 */
	public void dataChanged(int overs, int runs, int runsTowin, int ball, Player batsMan, int runMade, int wicket) {
		commentry.update(overs, runs, runsTowin, ball, batsMan, runMade, wicket);
		ScoreBoard sb = ScoreBoard.getInstance();
		Team team2 = sb.getTeam2();
		team2.setOvers(overs);
		team2.setRuns(runs);
		team2.setBalls(ball);
		if(wicket > 0) {
			team2.setWickets(team2.getWickets() + 1);
		}
		List<Player> players = team2.getPlayers();
		runMade = batsMan.getRuns() + runMade;
		batsMan.setRuns(runMade);
		batsMan.setBalls(batsMan.getBalls()+1);
		players.set(batsMan.getId()-1, batsMan);
		sb.setTeam2(team2);
	}
	
	/**
	 * This method used to simulate last few overs left for team2 cricket match with the help of weighted 
	 * random number generation based on
	 * 
	 */
	public void startMatch() {
		ScoreBoard sb = ScoreBoard.getInstance();
		Team team2 = sb.getTeam2();
		int totalCurrentScore = team2.getRuns();
    	int runMade = 0;
    	int currentBatsManCount  = 8;
    	Player batsMan = team2.getPlayers().get(7); 
    	Player runner= team2.getPlayers().get(8);
    	
    	int runsTowin = sb.getRunsToWin();
    	boolean isAllOut = false;
    	Player temp = null;
    	for(int i=(AppConstants.CURRENT_OVER+1); i<=AppConstants.TOTAL_OVERS; i++) {   // From 17th over to 20 overs. (last 4 overs of the match simulation).
    		if(isAllOut == true) {
    			break;
    		}
			runsTowin = sb.totalTargetScore()-totalCurrentScore;
			System.out.println((AppConstants.TOTAL_OVERS-i+1) +" overs left. "+runsTowin +" runs to win");
			for(int j=1; j<=6; j++) {
    			try {
    				//Thread.sleep(2000); // sleeping 2 seconds
    				runMade = Utils.getRun(batsMan.getName());
    			}catch(Exception ex) {
    				runMade = 101; // treating as dot ball
    			}
    			if(j == 6) {		// check if last ball of the over
    				if(runMade >=100 && runMade <=200) {
    					// It's a dot ball. exchange batsman and runner
    					dataChanged(i, totalCurrentScore, runsTowin, j, batsMan, 0, 0);
    					temp = batsMan;
        				batsMan= runner;
        				runner = temp;
    				}
    				
    				if(runMade >=300 && runMade <=400) {		// check if batsman is out. if out, bring next batsman. Also exchange batsman and runner.
    					dataChanged(i, totalCurrentScore, runsTowin, j, batsMan, 0, 1);
    					batsMan = runner;
    					currentBatsManCount = currentBatsManCount +1;
    					if(team2.getPlayers().get(currentBatsManCount) != null) {
    						runner = team2.getPlayers().get(currentBatsManCount);
    					}else {
    						isAllOut = true;		// all are out. End the match
    						break;
    					}
    				}
    				if(runMade >=1 && runMade <=6) {		// check if run made
    					totalCurrentScore = totalCurrentScore + runMade;
    					dataChanged(i, totalCurrentScore, runsTowin, j, batsMan, runMade, 0);
    					if(runMade %2 == 0) {		// if batsman makes even run, batsMan = runner, runner=batsMan;
    						temp = batsMan;
            				batsMan = runner;
            				runner = temp;
    					}
    				}
    			}else {
    				if(runMade >=100 && runMade <=200) {		// check if it is dot ball
    					dataChanged(i, totalCurrentScore, runsTowin, j, batsMan, 0, 0);
    				}
    				if(runMade >=300 && runMade <=400) {		// check if ball is out
    					dataChanged(i, totalCurrentScore, runsTowin, j, batsMan, 0, 1);
        				currentBatsManCount = currentBatsManCount +1;
        				if(team2.getPlayers().get(currentBatsManCount) != null) {
        					batsMan = team2.getPlayers().get(currentBatsManCount);
    					}else {
    						isAllOut = true;		// all are out. End the match
    						break;
    					}
    				}
    				if(runMade >=1 && runMade <=6) {		// check if run made
    					totalCurrentScore = totalCurrentScore + runMade;
        				dataChanged(i, totalCurrentScore, runsTowin, j, batsMan, runMade, 0);
        				if(runMade %2 != 0) {		// batsman = runner, runner = batsman
            				temp = batsMan;
            				batsMan = runner;
            				runner = temp;
            			}
    				}
    			}
    			if(totalCurrentScore >= sb.totalTargetScore()) {
    				break;
    			}
    		}
			if(totalCurrentScore >= sb.totalTargetScore()) {
				break;
			}
			System.out.println("\n");
    	}
	}
}
