package com.simpl.assignment;

import java.io.Serializable;

import com.simpl.assignment.dto.Player;
import com.simpl.assignment.dto.Team;

public class ScoreBoard implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Team team1;
	private Team team2;
	private static ScoreBoard scoreBoard;
	
	private ScoreBoard() {
		super();
	}
	
	public static ScoreBoard getInstance() {
		if(scoreBoard == null) {
			scoreBoard = new ScoreBoard();
		}
		return scoreBoard;
	}
	
	public Team getTeam1() {
		return team1;
	}
	public void setTeam1(Team team1) {
		this.team1 = team1;
	}
	public Team getTeam2() {
		return team2;
	}
	public void setTeam2(Team team2) {
		this.team2 = team2;
	}
	
	public int getRunsToWin() {
		return (team1.getRuns() - team2.getRuns());
	}
	
	public int totalTargetScore() {
		return team1.getRuns();
	}
	
	public void displayStats(Team team) {
		for(Player player : team.getPlayers()) {
			StringBuilder sb = new StringBuilder();
			sb.append(player.getName());
			sb.append(" - ");
			sb.append(player.getRuns());
			if(player.getStatus()!=null && !player.getStatus().isEmpty() && !player.getStatus().equalsIgnoreCase("OUT")) {
				sb.append("*");
			}
			sb.append(" (").append(player.getBalls()).append(" balls)");
			System.out.println(sb.toString());
		}
	}
	
}
