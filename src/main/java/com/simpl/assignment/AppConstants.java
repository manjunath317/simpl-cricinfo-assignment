package com.simpl.assignment;

/**
 * This class used to define constants for cricket game. And for team1 and team2 pre-requisite assumptions.
 * @author Manjunath Jakkandi
 *
 */
public class AppConstants {
	
	public static final int TOTAL_OVERS = 20;
	public static final int CURRENT_OVER = 16;
	public static final int TEAM_1_TOTAL_SCORE = 120;
	public static final int TEAM_1_TOTAL_WICKETS = 10;
	public static final int TEAM_2_TOTAL_SCORE = 80;
	public static final int TEAM_2_TOTAL_WICKETS = 7;
	public static final String TEAM_1_NAME = "Chennai";
	public static final String TEAM_2_NAME = "Bengaluru";
	public static final int TEAM_1_ID = 1;
	public static final int TEAM_2_ID = 2;
	
}
