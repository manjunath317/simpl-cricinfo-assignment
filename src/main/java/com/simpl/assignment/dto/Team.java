package com.simpl.assignment.dto;

import java.io.Serializable;
import java.util.List;

/**
 * This class used to define the properties of team.
 * @author Manjunath Jakkandi
 *
 */
public class Team implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String name;
	private int id;
	private int runs;
	private int overs;
	private int balls;
	private int wickets;
	private List<Player> players;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRuns() {
		return runs;
	}
	public void setRuns(int runs) {
		this.runs = runs;
	}
	public int getOvers() {
		return overs;
	}
	public void setOvers(int overs) {
		this.overs = overs;
	}
	public int getBalls() {
		return balls;
	}
	public void setBalls(int balls) {
		this.balls = balls;
	}
	public List<Player> getPlayers() {
		return players;
	}
	public void setPlayers(List<Player> players) {
		this.players = players;
	}
	public Player getPlayer(int index) {
		return players.get(index);
	}
	public int getWickets() {
		return wickets;
	}
	public void setWickets(int wickets) {
		this.wickets = wickets;
	}
}
