package com.simpl.assignment.dto;

import java.io.Serializable;

public class Player implements Serializable{

	private static final long serialVersionUID = 1L;

	private int id;
	private String name;
	private int runs;
	private String status;
	private int balls;
	
	public Player(int id, String name, int runs, String status, int balls) {
		this.id = id;
		this.name = name;
		this.runs = runs;
		this.status = status;
		this.balls = balls;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getRuns() {
		return runs;
	}
	public void setRuns(int runs) {
		this.runs = runs;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getBalls() {
		return balls;
	}
	public void setBalls(int balls) {
		this.balls = balls;
	}
}
