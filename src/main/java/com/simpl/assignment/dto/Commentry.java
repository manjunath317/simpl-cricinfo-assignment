package com.simpl.assignment.dto;

import java.io.Serializable;

public class Commentry implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private int overs;
	private int totalCurrentScore;
	private int totalScoreNeedToWin;
	private int ball;
	private Player player;
	private int runMade;
	private int wicket;
	
	public void update(int overs, int totalCurrentScore, int totalScoreNeedToWin, int ball, Player player, int runMade, int wicket) {
		this.overs = overs;
		this.totalCurrentScore = totalCurrentScore;
		this.totalScoreNeedToWin = totalScoreNeedToWin;
		this.ball = ball;
		this.player = player;
		this.runMade = runMade;
		this.wicket = wicket;
		displayByBallToBall();
	}
	
	public void displayByOvers() {
		int runsToWin = totalScoreNeedToWin - totalCurrentScore;
		System.out.println(overs +" Overs left. "+runsToWin +" runs to win");
	}
	
	public void displayByBallToBall() {
		StringBuilder sb = new StringBuilder();
		if(wicket > 0) {
			sb.append(overs).append(".");
			sb.append(ball).append(" ").append(player.getName()+" is OUT");
		}else {
			sb.append(overs).append(".");
			sb.append(ball).append(" ").append(player.getName()+" scores ").append(runMade);
			if(runMade > 1 && runMade <=6) {
				sb.append(" runs");
			}
			if(runMade ==0 || runMade == 1) {
				sb.append(" run");
			}
		}
		System.out.println(sb.toString());
	}
	
	public int getOvers() {
		return overs;
	}
	public void setOvers(int overs) {
		this.overs = overs;
	}
	public int getTotalCurrentScore() {
		return totalCurrentScore;
	}
	public void setTotalCurrentScore(int totalCurrentScore) {
		this.totalCurrentScore = totalCurrentScore;
	}
	public int getTotalScoreNeedToWin() {
		return totalScoreNeedToWin;
	}
	public void setTotalScoreNeedToWin(int totalScoreNeedToWin) {
		this.totalScoreNeedToWin = totalScoreNeedToWin;
	}
	public int getBall() {
		return ball;
	}
	public void setBall(int ball) {
		this.ball = ball;
	}
	public Player getPlayer() {
		return player;
	}
	public void setPlayer(Player player) {
		this.player = player;
	}
	public int getRunMade() {
		return runMade;
	}
	public void setRunMade(int runMade) {
		this.runMade = runMade;
	}
	public int getWicket() {
		return wicket;
	}
	public void setWicket(int wicket) {
		this.wicket = wicket;
	}
}
