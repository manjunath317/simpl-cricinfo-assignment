package com.simpl.assignment.utils;

import java.util.ArrayList;
import java.util.List;

import com.simpl.assignment.AppConstants;
import com.simpl.assignment.ScoreBoard;
import com.simpl.assignment.dto.Player;
import com.simpl.assignment.dto.Team;

import net.andreinc.mockneat.MockNeat;

/**
 * This class used to set up pre-requisite team configurations along with assumptions.
 * @author Manjunath Jakkandi
 *
 */
public class Utils {
	
	public void intTeams() {
		Team team1 = initTeam1();
		Team team2 = initTeam2();
		
		ScoreBoard sb = ScoreBoard.getInstance();
		sb.setTeam1(team1);
		sb.setTeam2(team2);
	}

	public Team initTeam1() {
		Team team1 = new Team();
		team1.setId(AppConstants.TEAM_1_ID);
		team1.setName(AppConstants.TEAM_1_NAME);
		team1.setOvers(AppConstants.TOTAL_OVERS);
		team1.setRuns(AppConstants.TEAM_1_TOTAL_SCORE);
		team1.setWickets(AppConstants.TEAM_1_TOTAL_WICKETS);
		List<Player> team1Players = new ArrayList<Player>();
		Player player = null;
		player = new Player(1, "Player 1",0,"OUT", 1);
		team1Players.add(player);
		
		player = new Player(2, "Player 2",10,"OUT", 3);
		team1Players.add(player);
		
		player = new Player(3, "Player 3",10,"OUT", 3);
		team1Players.add(player);
		
		player = new Player(4, "Player 4",10,"OUT", 3);
		team1Players.add(player);
		
		player = new Player(5, "Player 5",10,"OUT", 3);
		team1Players.add(player);
		
		player = new Player(6, "Player 6",10,"OUT", 3);
		team1Players.add(player);
		
		player = new Player(7, "Player 7",10,"OUT", 3);
		team1Players.add(player);
		
		player = new Player(8, "Player 8",10,"OUT", 3);
		team1Players.add(player);
		
		player = new Player(9, "Player 9",10,"OUT", 3);
		team1Players.add(player);
		
		player = new Player(10, "Player 10",100,"OUT", 91);
		team1Players.add(player);
		
		player = new Player(11, "Player 11",20,"NOT_OUT", 4);
		team1Players.add(player);
		team1.setPlayers(team1Players);
		return team1;
	}
	
	public Team initTeam2() {
		Team team2 = new Team();
		team2.setId(AppConstants.TEAM_2_ID);
		team2.setName(AppConstants.TEAM_2_NAME);
		team2.setOvers(AppConstants.CURRENT_OVER);
		team2.setRuns(AppConstants.TEAM_2_TOTAL_SCORE);
		List<Player> team2Players = new ArrayList<Player>();
		Player player = null;
		player = new Player(1, "Player 1",10,"OUT", 3);
		team2Players.add(player);
		
		player = new Player(2, "Player 2",20,"OUT", 3);
		team2Players.add(player);
		
		player = new Player(3, "Player 3",20,"OUT", 3);
		team2Players.add(player);
		
		player = new Player(4, "Player 4",20,"OUT", 3);
		team2Players.add(player);
		
		player = new Player(5, "Player 5",20,"OUT", 3);
		team2Players.add(player);
		
		player = new Player(6, "Player 6",20,"OUT", 3);
		team2Players.add(player);
		
		player = new Player(7, "Player 7",50,"OUT", 72);
		team2Players.add(player);
		
		player = new Player(8, "Virat",0,"NOT_OUT", 0);
		team2Players.add(player);
		
		player = new Player(9, "Dhoni",0,"NOT_OUT", 0);
		team2Players.add(player);
		
		player = new Player(10, "Bumrah",0,"NOT_OUT", 0);
		team2Players.add(player);
		
		player = new Player(11, "Shashi",0,"NOT_OUT", 0);
		team2Players.add(player);
		team2.setPlayers(team2Players);
		team2.setWickets(7);
		return team2;
	}
	
	
	public static Integer getRun(String player) {
    	MockNeat m  = MockNeat.threadLocal();
    	Integer x = 0;
    	if(player==null || player.isEmpty()) {
    		return 0;
    	}
    	switch(player) {
    	case "Virat":
    		x = m.probabilites(Integer.class)
                .add(0.05, m.ints().range(100, 200)) // dot ball
                .add(0.30, m.ints().range(1, 6)) // 1st ball
                .add(0.25, m.ints().range(1, 6)) // 2nd ball
                .add(0.10, m.ints().range(1, 6)) // 3rd ball
                .add(0.15, m.ints().range(1, 6)) // 4th ball
                .add(0.01, m.ints().range(1, 6)) // 5th ball
                .add(0.09, m.ints().range(1, 6)) // 6th ball
                .add(0.05, m.ints().range(300, 400)) // out
                .val();
    		return x;
    	case "Dhoni":
    		x = m.probabilites(Integer.class)
	            .add(0.10, m.ints().range(100, 200)) 
	            .add(0.40, m.ints().range(1, 6)) 
	            .add(0.20, m.ints().range(1, 6)) 
	            .add(0.05, m.ints().range(1, 6)) 
	            .add(0.10, m.ints().range(1, 6)) 
	            .add(0.01, m.ints().range(1, 6)) 
	            .add(0.04, m.ints().range(1, 6)) 
	            .add(0.10, m.ints().range(300, 400)) 
	            .val();
    		return x; 
        case "Bumrah":
        	x = m.probabilites(Integer.class)
	            .add(0.20, m.ints().range(100, 200)) 
	            .add(0.30, m.ints().range(1, 6)) 
	            .add(0.15, m.ints().range(1, 6)) 
	            .add(0.05, m.ints().range(1, 6)) 
	            .add(0.05, m.ints().range(1, 6)) 
	            .add(0.01, m.ints().range(1, 6)) 
	            .add(0.04, m.ints().range(1, 6)) 
	            .add(0.20, m.ints().range(300, 400)) 
	            .val();
        	return x;
        case "Shashi":
        	x = m.probabilites(Integer.class)
	            .add(0.30, m.ints().range(100, 200)) 
	            .add(0.20, m.ints().range(1, 6)) 
	            .add(0.05, m.ints().range(1, 6)) 
	            .add(0.01, m.ints().range(100, 200)) 
	            .add(0.05, m.ints().range(1, 6)) 
	            .add(0.01, m.ints().range(1, 6)) 
	            .add(0.03, m.ints().range(1, 6)) 
	            .add(0.30, m.ints().range(300, 400)) 
	            .val();
        	return x;
        default: 
        	System.out.println("No match found for player "+player); 
            return x;
    	}
    	
    }
	
	
	public static int convertBallsRemaining(int overs, int balls) {
		int ballsRemaining = 0 ;
		int oversRemaining = AppConstants.TOTAL_OVERS - overs;
		ballsRemaining = (oversRemaining * 6) + balls;
		return ballsRemaining;
	}

}
