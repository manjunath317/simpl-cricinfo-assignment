package com.simpl.assignment;


import com.simpl.assignment.dto.Commentry;
import com.simpl.assignment.dto.Team;
import com.simpl.assignment.utils.Utils;

/**
 * 
 * @author Manjunath Jakkandi
 *
 */
public class App {
	
	public static void main(String[] args){
		
		initTeams();
		displayCommentry();
		displayScoreBoard();
    }
    	
	/**
	 * Init the teams and configure default values of team1 and team2.
	 */
    public static void initTeams() {
		Utils cricInfoUtils = new Utils();
		cricInfoUtils.intTeams();
	}
    
    /**
     * Used to display commentry details ball by ball.
     */
    public static void displayCommentry() {
		Commentry c = new Commentry();
		SimplCricInfo sci = new SimplCricInfo(c);
		System.out.println("---------------------------------------------------");
    	System.out.println("Commentry");
    	System.out.println("---------------------------------------------------");
		sci.startMatch();
	}
	
    /**
     * Used to display the details of team 2 score board
     */
	public static void displayScoreBoard() {
		ScoreBoard sb = ScoreBoard.getInstance();
		Team team2 = sb.getTeam2();
		Team team1 = sb.getTeam1();
		System.out.println("\n");
    	System.out.println("---------------------------------------------------");
    	System.out.println("ScoreBoard :: "+team1.getName() +" Vs "+team2.getName());
    	System.out.println("---------------------------------------------------");
    	System.out.println(team1.getName() +" Total  Score :: "+team1.getRuns());
    	System.out.println(team2.getName() +" Total  Score :: "+team2.getRuns());
    	StringBuilder sBuilder = new StringBuilder();
    	if(team1.getRuns() > team2.getRuns()) {
    		sBuilder.append(team1.getName());
			sBuilder.append(" won by ");
			sBuilder.append((team1.getRuns() - team2.getRuns()));
			sBuilder.append(" runs");
    	}else if(team1.getRuns() < team2.getRuns()){
    		int ballsRemaing = Utils.convertBallsRemaining(team2.getOvers(), team2.getBalls());
    		sBuilder.append(team2.getName());
			sBuilder.append(" won by ");
			sBuilder.append(11 - team2.getWickets());
			sBuilder.append(" wickets");
    		if(ballsRemaing > 0) {
    			sBuilder.append(" and ");
    			sBuilder.append(ballsRemaing);
    			sBuilder.append(" balls remaining");
    		}
    	}else {
    		sBuilder.append("It's a Draw");
    	}
    	System.out.println(sBuilder.toString());
    	sb.displayStats(team2);
	}
    
}
